use anchor_lang::prelude::*;

#[account]
pub struct PoolAccount {
    pub seed: u64,
    pub bump: u8,
    pub is_initialized: bool,
    pub seller_pubkey: Pubkey,
    pub selling_mint: Pubkey,
    pub amount_sell: u64,
    pub token_price: u64,
}

impl Space for PoolAccount {
    const INIT_SPACE: usize = 8 + 8 + 1 + 1 + 32 + 32 + 8 + 8;
}

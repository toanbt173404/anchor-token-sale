use anchor_spl::{
    associated_token::AssociatedToken,
    token::{Mint, Token, TokenAccount, Transfer, transfer},
};
use anchor_lang::prelude::*;

use crate::account::PoolAccount;


#[derive(Accounts)]
#[instruction(seed: u64, sell_amount: u64, price: u64)]
pub struct InitializeSale<'info> {
    #[account(mut)]
    pub seller: Signer<'info>,
    pub selling_mint: Account<'info, Mint>,
    #[account(
        init_if_needed,
        payer = seller,
        space = PoolAccount::INIT_SPACE,
        seeds = [b"token_sale".as_ref(), &seed.to_le_bytes()],
        bump
    )]
    pub pool_account: Account<'info, PoolAccount>,
    #[account(
        mut,
        constraint = seller_token_account.amount >= sell_amount,
        associated_token::mint = selling_mint,
        associated_token::authority = seller
    )]
    pub seller_token_account: Account<'info, TokenAccount>,
    #[account(
        init_if_needed,
        payer = seller,
        associated_token::mint = selling_mint,
        associated_token::authority = pool_account
    )]
    pub vault_selling: Account<'info, TokenAccount>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,

}


impl<'info> InitializeSale<'info> {
    pub fn initialize_token_sale(
        &mut self,
        seed: u64,
        bumps: &InitializeSaleBumps,
        amount_sell: u64,
        token_price: u64,
    ) -> Result<()> {
        self.pool_account.set_inner(PoolAccount {
            seed,
            bump: bumps.pool_account,
            is_initialized: true,
            seller_pubkey: self.seller.key(),
            selling_mint: self.selling_mint.key(),
            amount_sell,
            token_price,
        });
        Ok(())
    }

    pub fn send_tokens_to_pool(&self, amount_to_sell: u64) -> Result<()> {
        let cpi_accounts = Transfer {
            from: self.seller_token_account.to_account_info(),
            to: self.vault_selling.to_account_info(),
            authority: self.seller.to_account_info(),
        };
        transfer(
            CpiContext::new(self.token_program.to_account_info(), cpi_accounts),
            amount_to_sell.into()
        )
    }
}





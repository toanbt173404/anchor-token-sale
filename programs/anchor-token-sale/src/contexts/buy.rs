use anchor_spl::{
    associated_token::AssociatedToken,
    token::{Mint, Token, TokenAccount, transfer, Transfer},
};
use anchor_lang::prelude::*;
use crate::account::PoolAccount;

#[derive(Accounts)]
#[instruction(seed: u64, buy_amount: u64)]
pub struct BuyToken<'info> {
    #[account(mut)]
    pub buyer: Signer<'info>,
    pub selling_mint: Account<'info, Mint>,
    #[account(
        mut,
        seeds = [b"token_sale".as_ref(), &seed.to_le_bytes()],
        bump = pool_account.bump,
    )]
    pub pool_account: Account<'info, PoolAccount>,
    #[account(mut)]
    pub vault_selling: Account<'info, TokenAccount>,
    #[account(
        init_if_needed,
        payer = buyer,
        associated_token::mint = selling_mint,
        associated_token::authority = buyer
    )]
    pub buyer_token_account: Account<'info, TokenAccount>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,

}


impl<'info> BuyToken<'info> {
    pub fn send_tokens_from_pool_to_buyer(&self, tokens_amount: u64) -> Result<()> {
        let seeds = &[
            b"token_sale",
            &self.pool_account.seed.to_le_bytes()[..],
            &[self.pool_account.bump]
        ];

        transfer(
            CpiContext::new_with_signer(
                self.token_program.to_account_info(),
                Transfer {
                    from: self.vault_selling.to_account_info(),
                    to: self.buyer_token_account.to_account_info(),
                    authority: self.pool_account.to_account_info(),
                },
                &[&seeds[..]]
            ),
            tokens_amount.into()
        )
    }
}

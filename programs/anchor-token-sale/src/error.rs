use anchor_lang::prelude::*;

#[error_code]
pub enum ErrorCode {
    #[msg("Amount of tokens for sale is larger than token amount inside `tokens_for_distribution` account.")]
    NotEnoughTokensForSale,
    #[msg("Not enough tokens in the vault.")]
    InsufficientTokensInVault,
    #[msg("Not enough lamports to buy the requested amount of tokens.")]
    InsufficientLamportsToBuyTokens,
}

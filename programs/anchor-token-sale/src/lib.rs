use anchor_lang::prelude::*;
mod contexts;
use contexts::*;
mod account;
mod error; use error::ErrorCode;
mod helper; use helper::send_lamports;

declare_id!("DRFLjwGuhqgGsfV1uARA4sjQZ7DH7zxG93yUk1frkgt6");

#[program]
pub mod anchor_token_sale {
    use super::*;

    pub fn initialize_sale(ctx: Context<InitializeSale> ,seed: u64, sell_amount: u64, price: u64 ) -> Result<()> {
        ctx.accounts
        .initialize_token_sale(seed, &ctx.bumps, sell_amount, price)?;
        ctx.accounts.send_tokens_to_pool(sell_amount)?;

        Ok(())
    }

    pub fn buy(ctx: Context<BuyToken>, _seed: u64, amount_to_buy: u64) -> Result<()> {

        let amount_for_sale = ctx.accounts.pool_account.amount_sell;
        let lamports_amount = amount_to_buy.checked_mul(ctx.accounts.pool_account.token_price).unwrap();

        require!(amount_for_sale >= amount_to_buy, ErrorCode::InsufficientTokensInVault);

        let buyer_lamports = **ctx.accounts.buyer.to_account_info().try_borrow_lamports()?;

        require!(amount_for_sale >= amount_to_buy, ErrorCode::InsufficientTokensInVault);
        require!(buyer_lamports >= lamports_amount, ErrorCode::InsufficientLamportsToBuyTokens);

        let buyer = &mut ctx.accounts.buyer;
        let pool = &mut ctx.accounts.pool_account;

        send_lamports(buyer.to_account_info(), pool.to_account_info(), lamports_amount)?;
        ctx.accounts.send_tokens_from_pool_to_buyer(amount_to_buy)?;

        Ok(())
    }

}
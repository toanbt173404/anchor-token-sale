import * as anchor from "@coral-xyz/anchor";
import { Program } from "@coral-xyz/anchor";
import { AnchorTokenSale } from "../target/types/anchor_token_sale";
import { Connection, Keypair, LAMPORTS_PER_SOL, PublicKey, SystemProgram, Transaction, clusterApiUrl } from "@solana/web3.js";
import { ASSOCIATED_TOKEN_PROGRAM_ID, MINT_SIZE, TOKEN_PROGRAM_ID, createAssociatedTokenAccountIdempotentInstruction, createInitializeMint2Instruction, createMintToInstruction, getAssociatedTokenAddressSync, getMinimumBalanceForRentExemptMint } from "@solana/spl-token";
import { randomBytes } from "crypto";
const bs58 = require('bs58');

const TOKEN_DECIMAL = 6;

describe("anchor-token-sale", () => {
  anchor.setProvider(anchor.AnchorProvider.env());
  const provider = anchor.getProvider();
  const program = anchor.workspace.AnchorTokenSale as Program<AnchorTokenSale>;
  const connection = new Connection(clusterApiUrl("devnet"), "confirmed");

  // 1. Boilerplate
  // Determine dummy token mints and token account addresses
  const sellerTokenAccountPubkey = new PublicKey("7pceGYudQeSsf8aZ3y2wyH23k9k4xYcEskKgxJdKcDLg");
  const sellerPrivateKey = bs58.decode("4BU3ZSmLa5XhASXM2KZGZyC6KfFzzZDA7dpJcM9aerCfz8hyxMMxsa6TsmEuRjTBcKhFv2UwuAsx5V529kqLFvZi");

  const seller =  new Keypair({
    publicKey: sellerTokenAccountPubkey.toBytes(),
    secretKey: sellerPrivateKey,
  });

  const buyerTokenAccountPubkey = new PublicKey("5RhSPDNYnWyBhSBjjsfhnXf3kv7KgQPCZZHfKxpBkyyu");
  const buyerPrivateKey = bs58.decode("5zeG8sJKgVujevFv3jLNvybMrbWvCWpnJ8R2x1ToNcJi1tQEpB4JgNUxJ4WnRPPhL7zkv41MJGVFu5KeZwkWej2b");

  const buyer =  new Keypair({
    publicKey: buyerTokenAccountPubkey.toBytes(),
    secretKey: buyerPrivateKey,
  });

  console.log(buyer)


  const mint = Keypair.generate();
  const sellerAta = getAssociatedTokenAddressSync(mint.publicKey, seller.publicKey);

  const buyerAta = getAssociatedTokenAddressSync(mint.publicKey, buyer.publicKey);

  const seed = new anchor.BN(randomBytes(8));

  const pool_account = PublicKey.findProgramAddressSync(
    [Buffer.from("token_sale"), seed.toArrayLike(Buffer, "le", 8)],
    program.programId
  )[0];

  const vaultSelling = getAssociatedTokenAddressSync(mint.publicKey, pool_account, true);
  // 2. Utils
  // Account Wrapper
  const accounts = {
    seller: seller.publicKey,
    buyer: buyer.publicKey,
    sellingMint: mint.publicKey,
    poolAccount: pool_account,
    sellerTokenAccount: sellerAta,
    buyerTokenAccount: buyerAta,
    vaultSelling: vaultSelling,
    associatedTokenprogram: ASSOCIATED_TOKEN_PROGRAM_ID,
    tokenProgram: TOKEN_PROGRAM_ID,
    systemProgram: SystemProgram.programId,
  };

  const confirm = async (signature: string): Promise<string> => {
    const block = await connection.getLatestBlockhash();
    await connection.confirmTransaction({
      signature,
      ...block,
    });
    return signature;
  };

  const log = async (signature: string): Promise<string> => {
    console.log(
      `Your transaction signature: https://solscan.io/tx/${signature}?cluster=devnet`
    );
    return signature;
  };


  it("Create 1M token with decimal = 6 for seller", async () => {
    let lamports = await getMinimumBalanceForRentExemptMint(connection);
    let tx = new Transaction();
    console.log("Your transaction signature", tx);
    tx.instructions = [
      SystemProgram.transfer({
        fromPubkey: provider.publicKey,
        toPubkey: seller.publicKey,
        lamports: 0.01 * LAMPORTS_PER_SOL,
      }),
      SystemProgram.createAccount({
        fromPubkey: provider.publicKey,
        newAccountPubkey: mint.publicKey,
        lamports,
        space: MINT_SIZE,
        programId: TOKEN_PROGRAM_ID,
      }),
      ...[
        [mint.publicKey, seller.publicKey, sellerAta],
      ].flatMap((x) => [
        createInitializeMint2Instruction(x[0], TOKEN_DECIMAL, x[1], null),
        createAssociatedTokenAccountIdempotentInstruction(provider.publicKey, x[2], x[1], x[0]),
        createMintToInstruction(x[0], x[2], x[1], 1_000_000 * 10 ** TOKEN_DECIMAL),
      ]),
    ];
    await provider.sendAndConfirm(tx, [mint, seller]).then(log);
  });

  it("Initialize Sale", async () => {
    const sellAmount = 1_000 * 10 ** TOKEN_DECIMAL;
    const price = 0.001 * LAMPORTS_PER_SOL / (10** TOKEN_DECIMAL);
    await program.methods
      .initializeSale(seed, new anchor.BN(sellAmount), new anchor.BN(price))
      .accounts({ ...accounts })
      .signers([seller])
      .rpc()
      .then(confirm)
      .then(log);
  });

  it("Buy Token", async () => {
    const buyAmount = 10 * 10 ** TOKEN_DECIMAL;
    await program.methods
      .buy(seed, new anchor.BN(buyAmount))
      .accounts({ ...accounts })
      .signers([buyer])
      .rpc()
      .then(confirm)
      .then(log);
  });
});

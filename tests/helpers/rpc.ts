// import { PublicKey, Signer, SystemProgram } from "@solana/web3.js";
// import {
//     ASSOCIATED_TOKEN_PROGRAM_ID,
//     getOrCreateAssociatedTokenAccount,
//     TOKEN_PROGRAM_ID,
//     getAssociatedTokenAddress,
// } from "@solana/spl-token";
// import * as anchor from "@coral-xyz/anchor";
// import { Ctx } from "./ctx";
// import bs58 from 'bs58';

// export namespace RPC {
//     export async function initialize(ctx: Ctx) {
//         await ctx.program.methods.initializeSale(
//             ctx.roundStartAt,
//             ctx.endAt,
//             ctx.buyingDuration,
//             ctx.tradingDuration,
//             ctx.initialTokenPrice,
//             { tokens: ctx.amountForSale },
//             ctx.coeffA,
//             ctx.coeffB,
//         ).accounts({
//             poolAccount: ctx.accounts.pool.key,
//             distributionAuthority: ctx.owner.publicKey,
//             tokensForDistribution: ctx.tokensForDistribution.address,
//             sellingMint: ctx.sellingMint,
//             vaultSelling: ctx.vaultSelling,
//             systemProgram: SystemProgram.programId,
//             tokenProgram: TOKEN_PROGRAM_ID,
//             associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
//             clock: anchor.web3.SYSVAR_CLOCK_PUBKEY,
//         }).signers([ctx.owner]).rpc();
//     }

//     export async function buyTokens(ctx: Ctx, trader: Signer, tokensAmount: anchor.BN) {
//         const ata = await getOrCreateAssociatedTokenAccount(ctx.connection, trader, ctx.sellingMint, trader.publicKey);
//         await ctx.program.methods.buy(
//             { tokens: tokensAmount },
//         ).accounts({
//             poolAccount: ctx.accounts.pool.key,
//             sellingMint: ctx.sellingMint,
//             vaultSelling: ctx.vaultSelling,
//             buyer: trader.publicKey,
//             buyerTokenAccount: ata.address,
//             systemProgram: SystemProgram.programId,
//             tokenProgram: TOKEN_PROGRAM_ID,
//             clock: anchor.web3.SYSVAR_CLOCK_PUBKEY,
//         }).signers([trader]).rpc();
//     }



// }
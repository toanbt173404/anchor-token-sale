import {
    PublicKey,
    Connection,
    Signer,
    LAMPORTS_PER_SOL,
} from '@solana/web3.js';
import {
    createMint,
    getOrCreateAssociatedTokenAccount,
    Account as TokenAccount,
    mintTo,
    getAssociatedTokenAddress,
} from '@solana/spl-token';
import { Program } from "@coral-xyz/anchor";
import { AnchorTokenSale } from "../../target/types/anchor_token_sale";
import * as anchor from "@coral-xyz/anchor";
import {createUserWithLamports } from './helpers';


// This interface is passed to every RPC test functions
export interface Ctx {
    connection: Connection,
    program: Program<AnchorTokenSale>,
    // The owner of the IDO program and tokens
    owner: Signer,
    // The mint of the tokens that are selling in the IDO
    sellingMint: PublicKey,
    // Owner's ATA with tokens for IDO
    tokensForDistribution: TokenAccount,
    initialTokenPrice: anchor.BN,
    amountForSale: anchor.BN,
    // pool ATA for storing the IDO tokens
    vaultSelling: PublicKey,
    accounts: {
        pool: CtxAccountPDA,
    }
}

export interface CtxAccount {
    key: PublicKey,
}

export interface CtxAccountPDA extends CtxAccount {
    bump: number,
}

export async function createCtx(connection: Connection, program: Program<AnchorTokenSale>): Promise<Ctx> {
    const owner = await createUserWithLamports(connection, 1);
    const sellingMint = await createMint(
        connection,
        owner, // payer
        owner.publicKey, // mintAuthority
        owner.publicKey, // freezeAuthority
        6 // decimals
    );
    const tokensForDistribution = await getOrCreateAssociatedTokenAccount(connection, owner, sellingMint, owner.publicKey);
    const tokensForDistributionAmount = 10_000;
    await mintTo(
        connection,
        owner,
        sellingMint,
        tokensForDistribution.address,
        owner,
        tokensForDistributionAmount,
    );
    const [poolPDA, poolBump] = await anchor.web3.PublicKey.findProgramAddressSync(
        [sellingMint.toBuffer()],
        program.programId
    );
    const vaultSelling = await getAssociatedTokenAddress(sellingMint, poolPDA, true);

    return {
        connection,
        program,
        owner,
        sellingMint,
        tokensForDistribution,
        // 1 Token = 100_000_000 Lamports = 0.01 SOL
        initialTokenPrice: new anchor.BN(0.01 * LAMPORTS_PER_SOL),
        amountForSale: new anchor.BN(10_000),
        vaultSelling,
        accounts: {
            pool: { key: poolPDA, bump: poolBump },
        }
    }
}
